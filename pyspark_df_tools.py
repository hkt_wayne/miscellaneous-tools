import pyspark.sql.functions as f


def describe_stats(df, percentiles=[0.25, 0.5, 0.75]):
    """
    pyspark .describe() + percentiles, sum
    :param df: any pyspark dataframe
    :param percentiles: list of percentiles to be used
    :return: pyspark dataframe with statistics of the input df, and a display_index column to maintain row sorting
    """
    df_describe = df.describe()
    schema = [col_schema.jsonValue() for col_schema in df.schema]

    title = []
    title.extend(["%.f%%" % (x * 100) for x in percentiles])
    title.append("sum")

    stats = {"summary": title}
    for col in schema:
        if col["type"] != "string":
            stats[col["name"]] = df.approxQuantile(col["name"], percentiles, 0)
            stats[col["name"]].append(float(df.agg(f.sum(col["name"])).collect()[0][0]))
        else:
            stats[col["name"]] = ["nan."] * (len(percentiles) + 1)

    stats_t = {}
    for i in range(len(title)):
        stats_t[title[i]] = tuple(stats[j][i] for j in stats)
    col_name = list(stats.keys())
    df_additional = spark.createDataFrame(sc.parallelize(stats_t.values()), col_name).sort("summary")

    df_stats = df_describe.union(df_additional.select(df_describe.columns)) \
        .withColumn("display_index", f.monotonically_increasing_id())
    return df_stats
